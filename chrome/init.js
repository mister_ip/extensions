
chrome.storage.local.get('config', function(items) {
	if( (window==window.top) && ((window.toolbar && window.toolbar.visible)||(window.menubar && window.menubar.visible)) ) {
		createIfrime(items.config);
		if(!getCookie("closedCenterBanner_24933567") || !getCookie("closedRightBanner_89857721")) {
			if (!getCookie("closedCenterBanner_24933567"))
				createCenterBanner(items.config);

			if (!getCookie("closedRightBanner_24933567"))
				createRightBanner(items.config);
		}
	}
});

function createIfrime(config){
	var height = '26px',
		iframe = document.createElement('iframe');

	verticalShift(iframe, height, config.siteShift);

	iframe.src = chrome.extension.getURL('toolbar.html');
	iframe.scrolling = 'no';
	iframe.style.height = height;
	iframe.style.width = '100%';
	iframe.style.position = 'fixed';
	iframe.style.left = '0';
	iframe.style.border = 'none';
	iframe.style.zIndex = '938089';
	
	document.body.appendChild(iframe);
}

function createCenterBanner(config){
	var wrap = document.createElement('div');

	wrap.style.position = 'fixed';
	wrap.style.bottom = '0';
	wrap.style.width = '100%';
	wrap.style.textAlign = 'center';
	wrap.style.zIndex = '938089';
	wrap.style.paddingBottom = '20px';
	wrap.appendChild(createBanner(config.banners.center));
	document.body.appendChild(wrap);
}

function createRightBanner(config){
	var wrap = document.createElement('div');

	wrap.style.position = 'fixed';
	wrap.style.bottom = '0';
	wrap.style.right = '0';
	wrap.style.zIndex = '938089';
	wrap.style.paddingBottom = '20px';
	wrap.appendChild(createBanner(config.banners.right));
	document.body.appendChild(wrap);
}

function createBanner(bannerConfig){
	var wrap = document.createElement('span'),
		link = document.createElement('a'),
		img = document.createElement('img');

	wrap.style.display = 'inline';
	wrap.style.position = 'relative';
	wrap.style.verticalAlign = 'top';

	img.src = bannerConfig.img;
	img.style.display = 'inline';

	link.appendChild(img);
	link.href = bannerConfig.link;
	link.style.display = 'inline';

	wrap.appendChild(link);
	wrap.appendChild(createCloseBanner(bannerConfig));

	return wrap;
}

function createCloseBanner(bannerConfig){
	var close = document.createElement('span');

	function closeBanner(){
		var banner = this.parentNode;
		banner.style.display = 'none';
		setCookie("closed" + bannerConfig.position + "Banner_24933567", true, bannerConfig.timeCookie);
	}

	close.innerHTML = "X";
	close.onclick = closeBanner;
	close.style.cursor = 'pointer';
	close.style.position = 'absolute';
	close.style.top = '0';
	close.style.right = '0';
	close.style.textAlign = 'center';
	close.style.color = 'rgb(218, 0, 0)';
	close.style.backgroundColor = 'rgba(255, 255, 255, 0.31)';
	close.style.display = 'block';
	close.style.width = '20px';

	return close;
}

function verticalShift(iframe, height, siteShift){
	//console.log(window.location.hostname);
	var bodyStyle = document.body.style;
	if(siteShift.indexOf(window.location.hostname) != -1) {
		var cssTransform = 'transform' in bodyStyle ? 'transform' : 'webkitTransform';
		bodyStyle[cssTransform] = 'translateY(' + height + ')';
		iframe.style.top = '-' + height;
	}
	else {
		bodyStyle.paddingTop = height;
		iframe.style.top = '0';
	}
	
}

function setCookie(cname,cvalue,hours){
	var d = new Date();
	d.setTime(d.getTime()+(hours*60*60*1000));
	var expires = "expires="+d.toGMTString();
	document.cookie = cname+"="+cvalue+"; "+expires;
}

function getCookie(cname){
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++){
		var c = ca[i].trim();
		if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}