var urlConfigJson = "http://vmatveev.shpp.me/server/config.json",
	defaultConfig = {
		toolbar: {
			link: "",
			img: chrome.extension.getURL('link.png')
		},
		banner: {
			link: "http://www.healthypawspetinsurance.com/images/v2.2/CatsAndKittens.jpg",
			img: "http://www.healthypawspetinsurance.com/images/v2.2/CatsAndKittens.jpg",
			timeCookie: 1200
		}
	},
	config = defaultConfig,
	xhr = new XMLHttpRequest();

chrome.storage.local.get('config', function(items) {
	if (items.config)
		config = (items.config, config);
});

xhr.open("GET", urlConfigJson, false);
xhr.onreadystatechange = function(){
	if (xhr.readyState == 4) {
		config = merge(JSON.parse(xhr.responseText), config);
		chrome.storage.local.set({'config': config});//JSON.stringify(config)
	}
};
xhr.send();

function merge(obj1,obj2){
	var result = {};
	for(i in obj1){
		if((i in obj2) && (typeof obj1[i] === "object") && (i !== null)){
			result[i] = merge(obj1[i],obj2[i]);
		}else{
			result[i] = obj1[i];
		}
	}
	for(i in obj2){
		if(i in result){
			continue;
		}
		result[i] = obj2[i];
	}
	return result;
}
