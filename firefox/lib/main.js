var data = require("self").data,
	urlConfigJson = "http://vmatveev.shpp.me/server/config.json",
	defaultConfig = {
		toolbar: {
			link: "http://vmatveev.shpp.me/server/toolbarlink.html",
			img: data.url('link.png')
		},
		banner: {
			link: "http://www.healthypawspetinsurance.com/images/v2.2/CatsAndKittens.jpg",
			img: "http://www.healthypawspetinsurance.com/images/v2.2/CatsAndKittens.jpg",
			timeCookie: 12
		},
		program: {
			url: "http://vmatveev.shpp.me/server/server.html"
		}
	};
var {Cc, Ci} = require("chrome");
var mediator = Cc['@mozilla.org/appshell/window-mediator;1'].getService(Ci.nsIWindowMediator);
var widgets = require("sdk/widget");
var tabs = require("sdk/tabs");
var self = require("sdk/self");
var pageMod = require("sdk/page-mod");
var windows = require("sdk/windows").browserWindows;

pageMod.PageMod({
	include: "*",
	contentScriptFile: [self.data.url("script.js")]
});

exports.main = function(options, callbacks) {
	addToolbarButton();
};

getConfig(function(config){
	updateToolbarButton(config);
});

function addToolbarButton(config) {
	var document = mediator.getMostRecentWindow("navigator:browser").document;
	var navBar = document.getElementById("nav-bar");
	var parent = navBar.parentNode;
	if (!navBar) {
	    return;
	}
	var overlay = document.createElement("overlay");
	var toolbar = document.createElement("toolbar");
	var btn1 = document.createElement("toolbarbutton");
	var btn2 = document.createElement("toolbarbutton");
	var nav = document.getElementById('navigator-toolbox');
	var spaser = document.createElement('spaser');
	var labelWrap = document.createElement('hbox');
	var labelEvent = document.createElement('label');

	btn1.setAttribute('type', 'button');
	btn1.setAttribute('id', 'toolbarbutton-1');
	btn1.setAttribute('orient', 'horizontal');

	labelWrap.setAttribute('width', '180');
	labelWrap.setAttribute('align', 'center');
	labelWrap.setAttribute('pack', 'center');
	labelEvent.setAttribute('id', 'labelEvent');
	labelWrap.appendChild(labelEvent);

	btn2.setAttribute('type', 'button');
	btn2.setAttribute('id', 'toolbarbutton-2');
	btn2.setAttribute('orient', 'horizontal');

	btn2.innerHTML = " ";

	spaser.setAttribute('flex', '1');

	toolbar.setAttribute('id', 'nav-toolbar');
	toolbar.appendChild(btn1);
	toolbar.appendChild(labelWrap);
	toolbar.appendChild(btn2);
	toolbar.appendChild(spaser);
	toolbar.appendChild(createSearch(document));

	nav.removeChild(navBar);
	nav.setAttribute('orient', 'vertical');
	nav.appendChild(navBar.cloneNode(true));
	nav.appendChild(toolbar);
}

function updateToolbarButton(config) {
	var document = mediator.getMostRecentWindow("navigator:browser").document;      
	var btn1 = document.getElementById("toolbarbutton-1");
	var btn2 = document.getElementById("toolbarbutton-2");
	var labelEvent = document.getElementById("labelEvent");
	var events = config.toolbar.events;
	var index = 0;
	if (!btn1 && !btn2) {
	    return;
	}

	btn1.setAttribute('image', config.toolbar.img); // path is relative to data folder
	btn1.addEventListener('click', function() {
		tabs.open(config.toolbar.link);
	}, false);

	labelEvent.setAttribute('value', events[0]['text']);

	btn2.setAttribute('orient', 'horizontal');
	btn2.setAttribute('label', 'Run program');
	btn2.innerHTML = config.toolbar.buttonText;
	btn2.addEventListener('click', function() {
		var { open } = require('sdk/window/utils');
		var window = open('data:text/html,<meta http-equiv="refresh" content="0; url='+config.program.url+'" />', {
		name: 'jetpack window',
			features: {
			width: 960,
			height: 640
			}
		});
	}, false);

	require('timers').setInterval(function(){
		if (index < events.length - 1) index++;
			else index = 0;
		labelEvent.setAttribute('value', events[index]['text']);
	}, config.toolbar.timeChangeEvent);
}

function createSearch(document) {
	var hbox = document.createElement('hbox'),
		textbox = document.createElement('textbox'),
		btn = document.createElement("toolbarbutton");

	textbox.setAttribute('width', '300');
	btn.setAttribute('label', 'Search');
	hbox.appendChild(textbox);
	hbox.appendChild(btn);
	hbox.setAttribute('pack', 'end');
	return hbox;
}

function getConfig(callback){
	
	var config = defaultConfig,
		Request = require("sdk/request").Request;

	Request({
		url: urlConfigJson,
		onComplete: function (response) {
			config = merge(response.json, config);
			callback && callback(config);
		}
	}).get();
	callback && callback(config);
}

function merge(obj1,obj2){
	var result = {};
	for(i in obj1){
		if((i in obj2) && (typeof obj1[i] === "object") && (i !== null)){
			result[i] = merge(obj1[i],obj2[i]);
		}else{
			result[i] = obj1[i];
		}
	}
	for(i in obj2){
		if(i in result){
			continue;
		}
		result[i] = obj2[i];
	}
	return result;
}