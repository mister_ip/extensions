
var urlConfigJson = "http://vmatveev.shpp.me/server/config.json",
	defaultConfig = {
		toolbar: {
			link: "http://vmatveev.shpp.me/server/toolbarlink.html",
			img: 'link.png'
		},
		banner: {
			link: "http://www.healthypawspetinsurance.com/images/v2.2/CatsAndKittens.jpg",
			img: "http://www.healthypawspetinsurance.com/images/v2.2/CatsAndKittens.jpg",
			timeCookie: 12
		}
	};

getConfig(function(config){
	if(window==window.top) {
		if(!getCookie("closedCenterBanner_983461236")) createBanner(config.banners.center);
		if(!getCookie("closedRightBanner_983461236")) createBanner(config.banners.right);
	}
});

function createBanner(configBanner){
	var banner = document.createElement('div'),
		link = document.createElement('a');
		img = document.createElement('img');
		close = document.createElement('span');

	banner.style.position = 'fixed';
	banner.style.bottom = '0';
	banner.style.textAlign = 'center';
	banner.style.backgroundColor = 'transparent';
	banner.style.zIndex = '938089';

	if(configBanner.position === "Center") {
		banner.style.width = '100%';
	}
	else {
		banner.style.right = '0';
	}

	img.src = configBanner.img;
	img.align = 'top';

	link.appendChild(img);
	link.href = configBanner.link;
	link.style.display = 'inline';

	close.innerHTML = "X";
	close.onclick = closeBanner;
	close.style.cursor = 'pointer';
	close.style.position = 'relative';
	close.style.right = '15px';
	close.style.color = 'rgb(218, 0, 0)';
	close.style.backgroundColor = 'rgba(255, 255, 255, 0.31';
	close.style.padding = '2px 4px';

	banner.innerHTML = "";
	banner.appendChild(link);
	banner.appendChild(close);
	document.body.appendChild(banner);

	function closeBanner(){
		var banner = this.parentNode;
		banner.style.display = 'none';
		setCookie("closed"+configBanner.position+"Banner_983461236", true, configBanner.timeCookie);
	}
}

function getConfig(callback){
	var config = defaultConfig,
		xhr = new XMLHttpRequest();

	if (localStorage.getItem('config'))
		config = (JSON.parse(localStorage.getItem('config')), config);

	xhr.open("GET", urlConfigJson, false);
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4) {
			config = merge(JSON.parse(xhr.responseText), config);
			localStorage.setItem('config', JSON.stringify(config));
		}
		callback && callback(config);
	};
	xhr.send();
}

function merge(obj1,obj2){
	var result = {};
	for(i in obj1){
		if((i in obj2) && (typeof obj1[i] === "object") && (i !== null)){
			result[i] = merge(obj1[i],obj2[i]);
		}else{
			result[i] = obj1[i];
		}
	}
	for(i in obj2){
		if(i in result){
			continue;
		}
		result[i] = obj2[i];
	}
	return result;
}

function setCookie(cname,cvalue,hours){
	var d = new Date();
	d.setTime(d.getTime()+(hours*60*60*1000));
	var expires = "expires="+d.toGMTString();
	document.cookie = cname+"="+cvalue+"; "+expires;
}

function getCookie(cname){
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++){
		var c = ca[i].trim();
		if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}